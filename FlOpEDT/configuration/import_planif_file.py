# coding: utf-8

# 4 colonnes: nom (E), prenom (G), email (I), GroupeTP (AF)

import openpyxl

def import_planif_file():
	# Accès aux données
	path = "./etudiants_S1.xlsx"
	wb = openpyxl.load_workbook(path)
	sheet = wb.active

	#table
	tab_nom = []
	tab_pre = []
	tab_eml = []
	tab_grp = []

	for r in range(wb.max_row):
		if(sheet.cell(sheet.row(row=(r+1), column=9).value) is not None):
			tab_nom.append(sheet.cell(row=(r+1), column=5).value)
			tab_pre.append(sheet.cell(row=(r+1), column=7).value)
			tab_eml.append(sheet.cell(row=(r+1), column=9).value)
			tab_nom.append(sheet.cell(row=(r+1), column=32).value)
		else:
			break

	tab = [tab_nom,tab_pre,tab_eml,tab_grp]
	return tab

if __name__ == '__main__':
	print(import_planif_file())
